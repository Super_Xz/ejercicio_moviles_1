package com.example.ejercicio_moviles_1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;

public class FotoDialogFragment extends DialogFragment {


    private ImageButton imageButton;
    private String rutaFoto;

    public FotoDialogFragment(String rutaFoto) {
        super();
        this.rutaFoto = rutaFoto;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = (View) inflater.inflate(R.layout.dialog_personalizado,container, false);
        imageButton = (ImageButton) vista.findViewById(R.id.dialogImageButton);
        Glide.with(vista).load(String.valueOf(rutaFoto)).placeholder(R.drawable.logoalmi).into(imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return vista;
    }

    public ImageButton getImageButton() {
        return imageButton;
    }
}
