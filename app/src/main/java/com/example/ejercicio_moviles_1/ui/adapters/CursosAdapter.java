package com.example.ejercicio_moviles_1.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ejercicio_moviles_1.R;
import com.example.ejercicio_moviles_1.ui.cursos.Cursos;

import java.util.List;

public class CursosAdapter extends ArrayAdapter {
    private Cursos cursos = null;

    public CursosAdapter(@NonNull Context context, int resource,Cursos cursos) {
        super(context, resource);
        this.cursos = cursos;
    }

    @Override
    public int getCount() {
        return cursos.getCursos().size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return cursos.getCursos().get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View vista = inflater.inflate(R.layout.curso_item,parent,false);
        TextView tvCurso = vista.findViewById(R.id.tvCurso);
        tvCurso.setText(cursos.getCursos().get(position).getNombre());
        return vista;
    }
}
