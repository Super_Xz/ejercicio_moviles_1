package com.example.ejercicio_moviles_1.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.ejercicio_moviles_1.FotoDialogFragment;
import com.example.ejercicio_moviles_1.MainActivity;
import com.example.ejercicio_moviles_1.R;
import com.example.ejercicio_moviles_1.ui.fotos.Fotos;
import com.example.ejercicio_moviles_1.ui.recyclerview.RecyclerViewHolder;
import com.google.android.gms.dynamic.SupportFragmentWrapper;

public class FotosAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private Fotos fotos;
    private ViewGroup parent;
    private FotoDialogFragment dialog = null;
    public FotosAdapter(int num_pestaña) {
        this.fotos = new Fotos(num_pestaña);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        this.parent = parent;
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {
        holder.getTextView().setText(String.valueOf(fotos.getFotos().get(position).getNombre()));
        Glide.with(parent).load(String.valueOf(fotos.getFotos().get(position).getPathFoto())).into(holder.getImageButton());
        holder.getImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new FotoDialogFragment(fotos.getFotos().get(position).getPathFoto());
                dialog.show(((MainActivity)((FrameLayout)parent.getParent()).getContext()).getSupportFragmentManager(),"Dialog");
                Toast.makeText(parent.getContext(),String.valueOf(fotos.getFotos().get(position).getNombre()),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.frame_textview;
    }

    @Override
    public int getItemCount() {
        return fotos.getFotos().size();
    }
}
