package com.example.ejercicio_moviles_1.ui.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.ejercicio_moviles_1.ui.exterior.ExteriorFragment;
import com.example.ejercicio_moviles_1.ui.interior.InteriorFragment;

public class TabsAdapter extends FragmentStatePagerAdapter {
    int nNumOfTabs;
    public TabsAdapter(@NonNull FragmentManager fm,int NoofTabs) {
        super(fm);
        this.nNumOfTabs = NoofTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                InteriorFragment interior = new InteriorFragment();
                return interior;
            case 1:
                ExteriorFragment exterior = new ExteriorFragment();
                return exterior;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return nNumOfTabs;
    }
}
