package com.example.ejercicio_moviles_1.ui.cursos;

public class Curso {
    private String nombre = "";

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Curso(String nombre) {
        this.nombre = nombre;
    }
}
