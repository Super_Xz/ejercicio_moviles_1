package com.example.ejercicio_moviles_1.ui.cursos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cursos implements Serializable {
    private List<Curso> cursos = new ArrayList<>();

    public Cursos() {
        rellenarCursos();
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public void rellenarCursos(){
        cursos.add(new Curso("DAM 1"));
        cursos.add(new Curso("DAM 2"));
        cursos.add(new Curso("SMR 1"));
        cursos.add(new Curso("SMR 2"));
        cursos.add(new Curso("GA 1"));
        cursos.add(new Curso("GA 2"));
    }

}
