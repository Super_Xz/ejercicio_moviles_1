package com.example.ejercicio_moviles_1.ui.exterior;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ejercicio_moviles_1.R;
import com.example.ejercicio_moviles_1.ui.adapters.FotosAdapter;
import com.example.ejercicio_moviles_1.ui.fotos.Fotos;


public class ExteriorFragment extends Fragment {

    private RecyclerView recyclerView;
    private ExteriorViewModel mViewModel;
    private Context contexto;

    public ExteriorFragment() {
        // Required empty public constructor
    }
    
    public static ExteriorFragment newInstance(String param1, String param2) {
        ExteriorFragment fragment = new ExteriorFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exterior, container, false);
        recyclerView = view.findViewById(R.id.exteriorRecyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(),3));
        mViewModel = ViewModelProviders.of(this).get(ExteriorViewModel.class);
        mViewModel.getfotos().observe(getViewLifecycleOwner(), new Observer<Fotos>() {
            @Override
            public void onChanged(Fotos fotos) {
                recyclerView.setAdapter(new FotosAdapter(2));
            }
        });
        return view;
    }
}