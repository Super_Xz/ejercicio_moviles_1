package com.example.ejercicio_moviles_1.ui.exterior;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.ejercicio_moviles_1.ui.cursos.Cursos;
import com.example.ejercicio_moviles_1.ui.fotos.Fotos;

public class ExteriorViewModel extends ViewModel {

    private MutableLiveData<Fotos> fotos;

    public ExteriorViewModel() {
        fotos = new MutableLiveData<>();
        Fotos fot = new Fotos(2);
        fotos.postValue(fot);
    }

    public MutableLiveData<Fotos> getfotos() {
        return fotos;
    }
}