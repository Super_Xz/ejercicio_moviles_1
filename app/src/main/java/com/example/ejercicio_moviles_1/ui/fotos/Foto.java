package com.example.ejercicio_moviles_1.ui.fotos;

public class Foto {
    private String nombre="";
    private String pathFoto="";

    public Foto(String nombre, String pathFoto) {
        this.nombre = nombre;
        this.pathFoto = pathFoto;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }
}
