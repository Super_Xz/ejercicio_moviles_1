package com.example.ejercicio_moviles_1.ui.fotos;

import com.example.ejercicio_moviles_1.ui.cursos.Curso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Fotos implements Serializable {

    private List<Foto> fotos = new ArrayList<>();

    public Fotos(int num_pestaña) {
        if (num_pestaña==1){
            rellenarFotosInterior();
        }else{
            rellenarFotosExterior();
        }
    }

    public List<Foto> getFotos() {
        return fotos;
    }

    public void setFotos(List<Foto> fotos) {
        this.fotos = fotos;
    }

    public void rellenarFotosExterior(){
        fotos.add(new Foto("Almi","https://almi.eus/wp-content/uploads/2019/11/IMG_20191125_084551.jpg"));
        fotos.add(new Foto("Almi2","https://pbs.twimg.com/media/DBTdveuW0AAF2Mq.jpg:large"));
        fotos.add(new Foto("Almi3","https://static.pacelma.es/wp-content/uploads/2013/04/BIZ.002.001.png"));
        fotos.add(new Foto("Almi4","https://static3.elcorreo.com/www/multimedia/201906/23/media/cortadas/almi1-kgMF-U80577704007VUE-624x385@El%20Correo.jpg"));
        fotos.add(new Foto("Almi5","https://almi.eus/wp-content/uploads/2016/09/03-Entrada-Almi-1024x576.jpg"));
        fotos.add(new Foto("Almi6","https://www.gentalia.eu/wp-content/uploads/2017/11/bidarte01.jpeg"));
        fotos.add(new Foto("Almi7","https://www.gentalia.eu/wp-content/uploads/2017/11/bidarte02-1024x561.jpeg"));
        fotos.add(new Foto("Almi8","https://www.gentalia.eu/wp-content/uploads/2017/11/bidarte03-1024x452.jpeg"));
        fotos.add(new Foto("Almi9","https://www.orange.es/ss/Satellite?blobcol=urldata&blobheadername1=Cache-Control&blobheadervalue1=max-age%3D3600&blobkey=id&blobnocache=false&blobtable=MungoBlobs&blobwhere=1521462942516&ssbinary=true"));
    }
    public void rellenarFotosInterior(){
        fotos.add(new Foto("Almi","https://almi.eus/wp-content/uploads/2017/05/IMG-20170516-WA0000.jpg"));
        fotos.add(new Foto("Almi2","https://almi.eus/wp-content/uploads/2016/12/IMG-20161220-WA0023.jpg"));
        fotos.add(new Foto("Almi3","https://almi.eus/wp-content/uploads/2018/06/Ethazi.jpg"));
        fotos.add(new Foto("Almi4","https://almi.eus/wp-content/uploads/2018/12/Escuelas-embajadoras-1024x768.jpeg"));
        fotos.add(new Foto("Almi5","https://www.ituna.eus/wp-content/uploads/2019/02/photo-2019-02-22-12-12-24.jpg"));
        fotos.add(new Foto("Almi6","https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRqV71ayK_q8Sp4bYoyMRFQLK6K7ES3IQ_veQ&usqp=CAU"));
        fotos.add(new Foto("Almi7","https://almi.eus/wp-content/uploads/2019/10/IMG20191016111605.jpg"));
        fotos.add(new Foto("Almi8","https://pbs.twimg.com/media/DLxDRA9X0AA9IWv.jpg"));
        fotos.add(new Foto("Almi9","https://pbs.twimg.com/media/DLxDRB4X4AAXvoF?format=jpg&name=large"));

    }
}
