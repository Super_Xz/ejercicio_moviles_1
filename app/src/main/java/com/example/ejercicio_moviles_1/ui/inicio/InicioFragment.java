package com.example.ejercicio_moviles_1.ui.inicio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.ejercicio_moviles_1.R;
import com.example.ejercicio_moviles_1.ui.adapters.CursosAdapter;
import com.example.ejercicio_moviles_1.ui.cursos.Cursos;

public class InicioFragment extends Fragment {

    private InicioViewModel mViewModel;
    private Context contexto;

    public static InicioFragment newInstance() {return new InicioFragment();}

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        contexto = context;

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Transition t1 = TransitionInflater.from(contexto).inflateTransition(android.R.transition.explode);
        t1.setDuration(500);
        setEnterTransition(t1);
        Transition t2 = TransitionInflater.from(contexto).inflateTransition(android.R.transition.fade);
        t2.setDuration(500);
        setExitTransition(t2);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inicio, container, false);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(InicioViewModel.class);
        //final ImageView ivLogoAlmi = getView().findViewById(R.id.ivLogoAlmi);
        final ListView lvCursos = getView().findViewById(R.id.lvCursos);
        mViewModel.getCursos().observe(getViewLifecycleOwner(), new Observer<Cursos>() {
            @Override
            public void onChanged(Cursos cursos) {
                lvCursos.setAdapter(new CursosAdapter(getContext(),R.layout.curso_item,cursos));
            }
        });
    }
}