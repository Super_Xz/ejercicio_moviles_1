package com.example.ejercicio_moviles_1.ui.inicio;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.ejercicio_moviles_1.ui.cursos.Cursos;

public class InicioViewModel extends ViewModel {

    private MutableLiveData<Cursos> cursos;

    public InicioViewModel() {
        cursos = new MutableLiveData<>();
        Cursos cur = new Cursos();
        cursos.postValue(cur);
    }

    public MutableLiveData<Cursos> getCursos() {
        return cursos;
    }
}