package com.example.ejercicio_moviles_1.ui.interior;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.ejercicio_moviles_1.ui.fotos.Fotos;

public class InteriorViewModel extends ViewModel {
    private MutableLiveData<Fotos> fotos;

    public InteriorViewModel() {
        fotos = new MutableLiveData<>();
        Fotos fot = new Fotos(1);
        fotos.postValue(fot);
    }

    public MutableLiveData<Fotos> getfotos() {
        return fotos;
    }
}
