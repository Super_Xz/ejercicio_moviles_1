package com.example.ejercicio_moviles_1.ui.recyclerview;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ejercicio_moviles_1.R;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    private TextView textView;
    private ImageButton imageButton;

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.randomText);
        imageButton = itemView.findViewById(R.id.recyclerImage);
    }
    public TextView getTextView() {
        return textView;
    }

    public ImageButton getImageButton() { return imageButton; }
}
